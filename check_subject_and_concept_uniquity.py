from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")


s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)

d_file_path_by_locale_and_subject = {}
d_file_path_by_locale_and_concept = {}
for p_file in p_content.rglob("*.tsv"):
    u_file = PurePath(p_file)
    s_file = u_file.name
    s_file_path = str(p_file)

    if s_file.startswith("• "):
        with open(s_file_path) as w_tsv:
            r_tsv = reader(w_tsv, delimiter="\t")
            l_header_component = next(r_tsv)
            for l_line_component in r_tsv:
                s_locale, s_subject = l_line_component
                t_locale_and_subject = (s_locale, s_subject)
                if t_locale_and_subject in d_file_path_by_locale_and_subject.keys():
                    l_existing_file_path = d_file_path_by_locale_and_subject[
                        t_locale_and_subject
                    ]
                    l_existing_file_path.append(s_file_path)
                    d_entry = {t_locale_and_subject: l_existing_file_path}
                    d_file_path_by_locale_and_subject.update(d_entry)
                else:
                    d_file_path_by_locale_and_subject[t_locale_and_subject] = [
                        s_file_path
                    ]
        continue

    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        for l_line_component in r_tsv:
            s_locale, s_concept, s_prerequisite = l_line_component
            if s_locale == "zxx-Zmth-ZZ":
                continue
            t_locale_and_concept = (s_locale, s_concept)
            if t_locale_and_concept in d_file_path_by_locale_and_concept.keys():
                l_existing_file_path = d_file_path_by_locale_and_concept[
                    t_locale_and_concept
                ]
                l_existing_file_path.append(s_file_path)
                d_entry = {t_locale_and_concept: l_existing_file_path}
                d_file_path_by_locale_and_concept.update(d_entry)
            else:
                d_file_path_by_locale_and_concept[t_locale_and_concept] = [s_file_path]

for t_locale_and_subject, l_file_path in d_file_path_by_locale_and_subject.items():
    i_file_path_amount = len(l_file_path)
    if i_file_path_amount > 1:
        print(t_locale_and_subject)
        print(l_file_path)

for t_locale_and_concept, l_file_path in d_file_path_by_locale_and_concept.items():
    i_file_path_amount = len(l_file_path)
    if i_file_path_amount > 1:
        print(t_locale_and_concept)
        print(l_file_path)
