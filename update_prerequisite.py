from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")

s_setting = "setting.yaml"
p_setting = Path(s_setting)
s_setting_text = p_setting.read_text()
d_setting = yaml.load(s_setting_text)

s_principal_locale = d_setting["principal locale"]
s_content_directory = d_setting["content directory"]

d_locale_and_concept_by_concept_in_principal_locale = {}
p_content = Path(s_content_directory)
for p_file in p_content.rglob("*.tsv"):
    u_file = PurePath(p_file)
    s_file = u_file.name
    if s_file.endswith("• "):
        continue

    s_file_path = str(p_file)
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        i_locale_index = l_header_component.index("locale")
        for l_line_component in r_tsv:
            s_locale = l_line_component[i_locale_index]
            if s_locale != s_principal_locale:
                continue
            (
                s_principal_locale,
                s_concept_in_principal_locale,
                s_prerequisite_in_principal_locale,
            ) = l_line_component
            if s_concept_in_principal_locale == "":
                print(s_file_path)
                print()

    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        s_header = "\t".join(l_header_component)

        for l_line_component in r_tsv:
            s_locale, s_concept, s_prerequisite = l_line_component
            if (
                s_concept_in_principal_locale
                in d_locale_and_concept_by_concept_in_principal_locale.keys()
            ):
                d_locale_and_concept_by_concept_in_principal_locale[
                    s_concept_in_principal_locale
                ].update({s_locale: s_concept})
            else:
                d_locale_and_concept_by_concept_in_principal_locale[
                    s_concept_in_principal_locale
                ] = {s_locale: s_concept}

for p_file in p_content.rglob("*.tsv"):
    u_file = PurePath(p_file)
    s_file = u_file.name
    if s_file.endswith("• "):
        continue

    s_file_path = str(p_file)
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        s_header = "\t".join(l_header_component)

        for l_line_component in r_tsv:
            s_locale, s_concept, s_prerequisite = l_line_component
            if s_locale != s_principal_locale:
                continue
            l_prerequisite_component_in_principal_locale = s_prerequisite.split(" _ ")
            break
    if l_prerequisite_component_in_principal_locale == [""]:
        continue

    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        s_header = "\t".join(l_header_component)

        l_line = []
        for l_line_component in r_tsv:
            s_locale, s_concept, s_prerequisite = l_line_component
            l_prerequisite_component = s_prerequisite.split(" _ ")

            l_new_prerequisite_component = []
            for (
                s_prerequisite_component_in_principal_locale
            ) in l_prerequisite_component_in_principal_locale:
                if (
                    s_prerequisite_component_in_principal_locale
                    in d_locale_and_concept_by_concept_in_principal_locale.keys()
                    and s_locale
                    in d_locale_and_concept_by_concept_in_principal_locale[
                        s_prerequisite_component_in_principal_locale
                    ].keys()
                ):
                    s_concept_as_prerequisite = (
                        d_locale_and_concept_by_concept_in_principal_locale[
                            s_prerequisite_component_in_principal_locale
                        ][s_locale]
                    )
                else:
                    s_concept_as_prerequisite = ""
                l_new_prerequisite_component.append(s_concept_as_prerequisite)
            s_new_prerequisite = " _ ".join(l_new_prerequisite_component)
            l_line_component = [s_locale, s_concept, s_new_prerequisite]
            s_line = "\t".join(l_line_component)
            l_line.append(s_line)
        s_new_text = s_header + "\n" + "\n".join(l_line) + "\n"

        s_text = p_file.read_text()
        if s_new_text == s_text:
            continue
        s_file_path = str(p_file)
        p_file.write_text(s_new_text)
