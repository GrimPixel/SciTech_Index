from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")

s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_principal_locale = d_setting["principal locale"]
s_content_directory = d_setting["content directory"]
p_content = Path(s_content_directory)
for p_file in p_content.rglob("*.tsv"):
    s_file_path = str(p_file)
    with open(s_file_path) as w_tsv:
        r_tsv = reader(w_tsv, delimiter="\t")
        l_header_component = next(r_tsv)
        i_locale_index = l_header_component.index("locale")

        u_file = PurePath(p_file)
        s_file = u_file.name
        for l_line_component in r_tsv:
            s_locale = l_line_component[i_locale_index]
            if s_locale != s_principal_locale:
                continue

            if s_file.startswith("• "):
                s_principal_locale, s_subject_in_principal_locale = l_line_component
                if s_subject_in_principal_locale == "":
                    print(s_file_path)
                    print()
                    continue
                s_new_file = "• " + s_subject_in_principal_locale + ".tsv"
            else:
                (
                    s_principal_locale,
                    s_concept_in_principal_locale,
                    s_prerequisite_in_principal_locale,
                ) = l_line_component
                if s_concept_in_principal_locale == "":
                    print(s_file_path)
                    print()
                    continue
                s_new_file = s_concept_in_principal_locale + ".tsv"
            break
        if s_new_file == s_file:
            continue

        s_file_directory = u_file.parent
        p_file_directory = Path(s_file_directory)
        p_new_file = p_file_directory.joinpath(s_new_file)
        if p_new_file.is_file():
            print(s_file_path)
            exit()
        print(p_new_file)
        p_file.rename(p_new_file)
